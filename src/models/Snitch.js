import { observable, action } from 'mobx';
import Model from './Model';

class Snitch extends Model {
  @observable timeAppeared = null;
  @observable timeCaught = null;
  @observable caughtBy = '';

  constructor(doc) {
    super(doc);
    Object.assign(this, doc);
  }

  @action appeard() {
    if (this.timeAppeared === null) {
      this.timeAppeared = new Date(Date.now());
    } else {
      throw new Error('Snitch has already appeared');
    }
  }

  @action caught(player) {
    if (this.timeAppeared === null) {
      throw new Error('Snitch did not appear yet');
    } else {
      this.timeCaught = new Date(Date.now());
      this.caughtBy = player;
    }
  }
}

export default Snitch;
