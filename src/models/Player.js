import Model from './Model';

class Player extends Model {
  constructor(docs) {
    super(docs);
    Object.assign(this, docs);
    this.firstName = this.firstName;
    this.lastName = this.lastName;
    this.number = this.number;
    this.position = this.position;
    this.goalsMade = 0;
    this.goalsMissed = 0;
    this.goalsBlocked = 0;
    this.snitchCaught = false;
  }

  scoreGoal() {
    if (this.position === 'Chaser') {
      this.goalsMade += 1;
    } else {
      throw new Error('Only chasers can score a goal');
    }
  }

  missGoal() {
    if (this.position === 'Chaser') {
      this.goalsMissed += 1;
    } else {
      throw new Error('Only chasers can miss a goal');
    }
  }

  blockGoal() {
    if (this.position === 'Keeper') {
      this.goalsBlocked += 1;
    } else {
      throw new Error('Only keepers can block a goal');
    }
  }

  catchSnitch(snitch) {
    if (this.position === 'Seeker') {
      snitch.caught(this);
      this.goalsMade = 3;
      this.snitchCaught = true;
    } else {
      throw new Error('Only seekers can catch the snitch');
    }
  }
}

export default Player;
