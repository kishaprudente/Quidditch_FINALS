import Model from './Model';

class Game extends Model {
  constructor(doc) {
    super(doc);
    Object.assign(this, doc);
    this.startTime = null;
    this.endTime = null;
    this.teams = [];
    this.gameHistory = [{
      event: '',
      time: null,
      player: '',
    }];
  }

  startGame() {
    if (this.startTime === null) {
      const timeStarted = new Date(Date.now());
      this.startTime = timeStarted;
      const history = {
        event: 'Game started',
        time: timeStarted,
      };
      this.gameHistory.push(history);
    } else {
      throw Error('Game did not start yet');
    }
  }

  endGame(time, seeker) {
    if (this.endTime === null) {
      const timeEnded = new Date(Date.now());
      this.endTime = timeEnded;
      const history = {
        event: 'Game ended',
        time,
        player: seeker,
      };
      this.gameHistory.push(history);
    } else if (this.startTime === null) {
      throw new Error('Game did not start yet');
    } else if (this.endTime === time) {
      throw new Error('Game has already ended');
    } else if (this.snitch.caught === null) {
      throw new Error('Snitch has not been caught yet');
    }
  }

  snitchAppeared() {
    if (this.endTime != null) {
      throw new Error('Game already ended');
    } else {
      this.snitch.appeared();
      const history = {
        event: 'Snitch appered',
        time: this.snitch.timeAppeared,
      };
      this.gameHistory.push(history);
    }
  }

  snitchCaught(seeker) {
    if (this.startTime === null) {
      throw new Error('Game did not start yet');
    } else {
      seeker.catchSnitch();
      this.endGame(this.snitch.timeCaught, seeker);
    }
  }

  goalMade(chaser) {
    if (this.endTime != null) {
      throw new Error('Game already ended');
    } else {
      chaser.scoreGoal();
      const history = {
        event: 'Goal made',
        time: new Date(),
        player: chaser,
      };
      this.gameHistory.push(history);
    }
  }

  goalMissed(chaser) {
    if (this.endTime != null) {
      throw new Error('Game already ended');
    } else {
      chaser.missGoal();
      const history = {
        event: 'Goal missed',
        time: new Date(),
        player: chaser,
      };
      this.gameHistory.push(history);
    }
  }

  goalBlocked(keeper) {
    if (this.endTime != null) {
      throw new Error('Game already ended');
    } else {
      keeper.blockGoal();
      const history = {
        event: 'Goal blocked',
        time: new Date(),
        player: keeper,
      };
      this.gameHistory.push(history);
    }
  }
}

export default Game;
