import Joi from 'joi';

export default class Model {
  static get schema() {
    return {};
  }

  constructor(doc) {
    Object.assign(this, doc);
  }

  validate() {
    return Joi.validate(this, this.constructor.schema);
  }

  isValid() {
    return this.validate().error === null;
  }
}
