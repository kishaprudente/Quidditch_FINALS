import Joi from 'joi';
import Model from './Model';

class User extends Model {
  constructor(docs) {
    super(docs);
    Object.assign(this, docs);
  }

  static get schema() {
    return {
      username: Joi.string().alphanum().min(4).max(12),
      password: Joi.string().min(3).max(20),
      firstName: Joi.string(),
      lastName: Joi.string(),
    };
  }
}

export default User;
