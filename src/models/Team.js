import Model from './Model';

class Team extends Model {
  constructor(docs) {
    super(docs);
    Object.assign(this, docs);
    this.name = this.name;
    this.players = [];
    this.goalsMade = 0;
  }

  get score() {
    let score = 0;
    this.players.forEach((player) => {
      this.goalsMade += player.goalsMade;
    });
    score += this.goalsMade * 10;
    return score;
  }
}

export default Team;
