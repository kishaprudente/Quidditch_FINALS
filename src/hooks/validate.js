function validateHook() {
  return function validate(hook) {
    if (hook.type === 'after') {
      throw new Error('use validate as a before hook');
    } else {
      if (hook.data.isValid()) {
        return hook;
      }
      throw new Error(hook.data.validate().error);
    }
  };
}

export default validateHook;
