function transformHook(Model) {
  return function hook(details) {
    const key = details.type === 'before' ? 'data' : 'result';
    if (details[key] instanceof Array) {
      const transformData = details[key].map(element => new Model(element));
      details[key] = transformData;
    } else if (details[key] instanceof Object) {
      details[key] = new Model(details[key]);
    } else {
      return new Error('Error on Transform Hook');
    }
    return details;
  };
}

export default transformHook;
