import feathers from 'feathers';
import rest from 'feathers-rest';
import hooks from 'feathers-hooks';
import socketio from 'feathers-socketio';
import configuration from 'feathers-configuration';
import { MongoClient } from 'mongodb';
import bodyParser from 'body-parser';
import path from 'path';
import setupAllServices from './services/allServices';

const app = feathers();

app
  .configure(rest())
  .configure(socketio())
  .configure(hooks())
  .configure(configuration(path.join(process.cwd())))
  .use(bodyParser.urlencoded({ extended: true }))
  .use(bodyParser.json())
  .use(feathers.static(path.join(process.cwd(), 'public')));

const server = async () => {
  const db = await MongoClient.connect(app.get('mongoURI'));
  app.configure(setupAllServices(db));
  return app;
};

export default server;
