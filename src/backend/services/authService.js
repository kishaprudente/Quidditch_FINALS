import auth from 'feathers-authentication';
import jwt from 'feathers-authentication-jwt';
import local from 'feathers-authentication-local';

function authService() {
  return function plugin() {
    const app = this;
    app
      .configure(auth(app.get('auth')))
      .configure(local(app.get('local')))
      .configure(jwt())
      .service('api/authentication')
      .hooks({
        before: {
          find: [],
          get: [],
          create: [
            auth.hooks.authenticate(['jwt', 'local']),
          ],
          update: [],
          patch: [],
          remove: [
            auth.hooks.authenticate('jwt'),
          ],
          all: [],
        },
        after: {
          find: [],
          get: [],
          create: [],
          update: [],
          patch: [],
          remove: [],
        },
      });
  };
}

export default authService;
