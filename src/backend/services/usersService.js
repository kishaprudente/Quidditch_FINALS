import feathersMongo from 'feathers-mongodb';
import auth from 'feathers-authentication';
import local from 'feathers-authentication-local';

function userService(db) {
  return function plugin() {
    const app = this;
    app
      .use('api/users', feathersMongo({ Model: db.collection('users') }))
      .service('api/users')
      .hooks({
        before: {
          find: [
            auth.hooks.authenticate('jwt'),
          ],
          get: [],
          create: [
            local.hooks.hashPassword({ passwordField: 'password' }),
          ],
          update: [],
          patch: [],
          remove: [],
          all: [],
        },
        after: {
          all: [],
          find: [],
          get: [],
          create: [],
          update: [],
          patch: [],
          remove: [],
        },
      });
  };
}

export default userService;
