import usersService from './usersService';
import playersService from './playersService';
import teamsService from './teamsService';
import gamesService from './gamesService';
import authService from './authService';

function setupAllServices(db) {
  return function plugin() {
    const app = this;
    app
      .configure(usersService(db))
      .configure(playersService(db))
      .configure(teamsService(db))
      .configure(gamesService(db))
      .configure(authService());
  };
}

export default setupAllServices;
