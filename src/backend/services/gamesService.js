import feathersMongo from 'feathers-mongodb';
import { populate } from 'feathers-hooks-common';

const gamesSchema = {
  include: {
    service: 'api/teams',
    nameAs: 'teams',
    parentField: '_id',
    childField: 'gameId',
  },
};

function gamesService(db) {
  return function plugin() {
    const app = this;
    app
      .use('api/games', feathersMongo({ Model: db.collection('games') }))
      .service('api/games')
      .hooks({
        before: {
          find: [],
          get: [],
          create: [],
          update: [],
          patch: [],
          remove: [],
          all: [],
        },
        after: {
          all: [],
          find: [
            populate({ schema: gamesSchema }),
          ],
          get: [
            populate({ schema: gamesSchema }),
          ],
          create: [],
          update: [],
          patch: [],
          remove: [],
        },
      });
  };
}

export default gamesService;
