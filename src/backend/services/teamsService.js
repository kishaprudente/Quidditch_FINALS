import feathersMongo from 'feathers-mongodb';
import { populate } from 'feathers-hooks-common';

const teamSchema = {
  include: {
    service: '/api/players',
    nameAs: 'players',
    parentField: 'playerIds',
    childField: '_id',
  },
};

function teamsService(db) {
  return function plugin() {
    const app = this;
    app
      .use('api/teams', feathersMongo({ Model: db.collection('teams') }))
      .service('api/teams')
      .hooks({
        before: {
          find: [],
          get: [],
          create: [],
          update: [],
          patch: [],
          remove: [],
          all: [],
        },
        after: {
          all: [],
          find: [
            populate({ schema: teamSchema }),
          ],
          get: [
            populate({ schema: teamSchema }),
          ],
          create: [],
          update: [],
          patch: [],
          remove: [],
        },
      });
  };
}

export default teamsService;
