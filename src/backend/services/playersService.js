import feathersMongo from 'feathers-mongodb';

function playersService(db) {
  return function plugin() {
    const app = this;
    app
      .use('api/players', feathersMongo({ Model: db.collection('players') }))
      .service('api/players')
      .hooks({
        before: {
          find: [],
          get: [],
          create: [],
          update: [],
          patch: [],
          remove: [],
          all: [],
        },
        after: {
          all: [],
          find: [],
          get: [],
          create: [],
          update: [],
          patch: [],
          remove: [],
        },
      });
  };
}

export default playersService;
