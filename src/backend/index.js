import server from './server';

const startServer = async () => {
  const app = await server();
  app.listen(process.env.PORT || 8080, () => console.log('Connected @ PORT:8080'));
};

startServer();
