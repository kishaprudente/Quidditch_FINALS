import path from 'path';
import fs from 'fs';

const seeds = async () => {
  const collectionName = process.argv[2];
  const fullPath = path.join(process.cwd(), `src/backend/seeds/${collectionName}.js`);
  const fileExists = fs.existsSync(fullPath);

  if (fileExists) {
    const seedFunc = require(`./${collectionName}`).default;
    seedFunc()
      .then(() => {
        console.log(`Seeds for ${collectionName} successful!`);
        process.exit(0);
      })
      .catch((error) => {
        console.log('ERROR: ', error);
      });
  } else {
    console.log(`Seed file for ${collectionName} does not exist.`);
  }
};

seeds();
