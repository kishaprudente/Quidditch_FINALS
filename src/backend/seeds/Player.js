import faker from 'faker';
import { ObjectId } from 'mongodb';
import server from '../server';

const seed = async () => {
  const app = await server();
  const players = [{
    _id: ObjectId('12345678901234567890abc0'),
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    number: faker.random.number(99),
    position: 'Seeker',
  }, {
    _id: ObjectId('12345678901234567890abc1'),
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    number: faker.random.number(99),
    position: 'Keeper',
  }, {
    _id: ObjectId('12345678901234567890abc2'),
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    number: faker.random.number(99),
    position: 'Chaser',
  }, {
    _id: ObjectId('12345678901234567890abc3'),
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    number: faker.random.number(99),
    position: 'Chaser',
  }, {
    _id: ObjectId('12345678901234567890abc4'),
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    number: faker.random.number(99),
    position: 'Chaser',
  }, {
    _id: ObjectId('12345678901234567890abc5'),
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    number: faker.random.number(99),
    position: 'Seeker',
  }, {
    _id: ObjectId('12345678901234567890abc6'),
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    number: faker.random.number(99),
    position: 'Keeper',
  }, {
    _id: ObjectId('12345678901234567890abc7'),
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    number: faker.random.number(99),
    position: 'Chaser',
  }, {
    _id: ObjectId('12345678901234567890abc8'),
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    number: faker.random.number(99),
    position: 'Chaser',
  }, {
    _id: ObjectId('12345678901234567890abc9'),
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    number: faker.random.number(99),
    position: 'Chaser',
  }];

  const playersService = app.service('api/players');
  return Promise.all(players.map(player => playersService.create(player)));
};

export default seed;
