import faker from 'faker';
import { ObjectId } from 'mongodb';
import server from '../server';

const seed = async () => {
  const app = await server();
  const teams = [{
    _id: ObjectId('1234567890123456789abc01'),
    name: faker.address.country(),
    playerIds: [
      ObjectId('12345678901234567890abc0'),
      ObjectId('12345678901234567890abc1'),
      ObjectId('12345678901234567890abc2'),
      ObjectId('12345678901234567890abc3'),
      ObjectId('12345678901234567890abc4'),
    ],
    gameId: ObjectId('1234567890123456789abc00'),
  }, {
    _id: ObjectId('1234567890123456789abc02'),
    name: faker.address.country(),
    playerIds: [
      ObjectId('12345678901234567890abc5'),
      ObjectId('12345678901234567890abc6'),
      ObjectId('12345678901234567890abc7'),
      ObjectId('12345678901234567890abc8'),
      ObjectId('12345678901234567890abc9'),
    ],
    gameId: ObjectId('1234567890123456789abc00'),
  }];

  const teamsService = app.service('api/teams');
  return Promise.all(teams.map(team => teamsService.create(team)));
};

export default seed;
