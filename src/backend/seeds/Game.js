import { ObjectId } from 'mongodb';
import server from '../server';

const seed = async () => {
  const app = await server();
  const games = [{
    _id: ObjectId('1234567890123456789abc00'),
    name: 'Game 1',
  }];

  const gamesService = app.service('api/games');
  return Promise.all(games.map(game => gamesService.create(game)));
};

export default seed;
