import server from '../server';

const seed = async () => {
  const app = await server();
  const users = [{
    username: 'kisha',
    password: '1234',
    fistName: 'Kisha',
    lastName: 'Prudente',
  }];

  const usersService = app.service('api/users');
  return Promise.all(users.map(user => usersService.create(user)));
};

export default seed;
