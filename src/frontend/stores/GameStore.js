import { observable, action, runInAction, computed } from 'mobx';
import Game from '../../models/Game';

class GameStore {
  @observable games = [];
  @observable currentGame = new Game();
  @observable event = '';
  @observable time = null;
  @observable teams = [];

  constructor(rootStore, client) {
    this.rootStore = rootStore;
    this.client = client;
  }

  @action.bound async fetchGames() {
    try {
      this.games = [];
      const games = await this.client.service('api/games').find();
      runInAction(() => {
        this.games = games;
      });
    } catch (error) {
      console.log(error);
    }
  }

  @action.bound manipulateGame() {
    try {
      this.client.service('api/games')
        .patch('1234567890123456789abc00',
          {
            gameHistory: [{
              event: 'Game Started',
              time: new Date(),
              player: null,
            }],
          })
        .then(gameNow => this.setCurrentGame(gameNow));
      this.currentGame.startGame();
      console.log(this.currentGame);
    } catch (error) {
      console.log(error);
    }
  }

  @action.bound setCurrentGame(game) {
    this.currentGame = game;
  }

  init() {
    this.fetchGames();
  }
}

export default GameStore;
