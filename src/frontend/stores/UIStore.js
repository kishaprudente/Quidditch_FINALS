import { observable, action } from 'mobx';

class UIStore {
  @observable loginViewOpen = false;
  @observable isLoggedIn = false;
  @observable toastViewOpen = false;
  @observable scoreboardOpen = false;

  constructor(rootStore) {
    this.rootStore = rootStore;
  }

  @action.bound toggleLogin() {
    this.loginViewOpen = !this.loginViewOpen;
  }

  @action.bound handleLoggedInUser() {
    this.isLoggedIn = !this.isLoggedIn;
  }

  @action.bound toggleToast() {
    this.toastViewOpen = !this.toastViewOpen;
  }

  @action.bound toggleScoreboard() {
    this.scoreboardOpen = !this.scoreboardOpen;
  }
}

export default UIStore;
