import UIStore from './UIStore';
import UserStore from './UserStore';
import GameStore from './GameStore';
import TeamStore from './TeamStore';
import PlayerStore from './PlayerStore';

class RootStore {
  constructor(client) {
    this.uiStore = new UIStore(this);
    this.userStore = new UserStore(this, client);
    this.gameStore = new GameStore(this, client);
    this.teamStore = new TeamStore(this, client);
    this.playerStore = new PlayerStore(this, client);
  }
}

export default RootStore;
