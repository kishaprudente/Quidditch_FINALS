import { observable, action, runInAction, computed } from 'mobx';

class TeamStore {
  @observable teams = [];
  @observable players = [];

  constructor(rootStore, client) {
    this.rootStore = rootStore;
    this.client = client;
  }

  @action.bound async fetchTeams() {
    try {
      this.teams = [];
      this.players = [];
      const teams = await this.client.service('api/teams').find();
      runInAction(() => {
        this.teams = teams;
        // this.players = players;
      });
    } catch (error) {
      console.log(error);
    }
  }

  @computed get filterChasers() {
    return this.players.filter(player => player.position === 'Chaser');
  }

  @computed get filterKeepers() {
    return this.players.filter(player => player.position === 'Keeper');
  }

  @computed get filterSeekers() {
    return this.players.filter(player => player.position === 'Seeker');
  }

  init() {
    this.fetchTeams();
  }
}

export default TeamStore;
