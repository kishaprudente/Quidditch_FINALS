import { observable, action } from 'mobx';
import Joi from 'joi-browser';
import User from '../../models/User';

class UserStore {
  @observable loggedInUser = undefined;
  @observable values = {
    username: '',
    password: '',
  };
  @observable usernameError = '';
  @observable passwordError = '';

  constructor(rootStore, client) {
    this.rootStore = rootStore;
    this.client = client;
  }

  @action.bound setUsername(usernameValue) {
    Joi.validate({ username: usernameValue }, User.schema, (error) => {
      if (error) {
        const message = error.details.map(detail => detail.message);
        this.usernameError = message;
      } else {
        this.usernameError = '';
        this.values.username = usernameValue;
      }
    });
  }

  @action.bound setPassword(passwordValue) {
    Joi.validate({ password: passwordValue }, User.schema, (error) => {
      if (error) {
        const message = error.details.map(detail => detail.message);
        this.passwordError = message;
      } else {
        this.passwordError = '';
        this.values.password = passwordValue;
      }
    });
  }

  async loginUser() {
    try {
      const result = await this.client.authenticate({
        strategy: 'local',
        username: this.values.username,
        password: this.values.password,
      });
      const payload = await this.client.passport.verifyJWT(result.accessToken);
      const user = await this.client.service('/api/users').get(payload.userId);
      this.loggedInUser = user;
      this.rootStore.uiStore.handleLoggedInUser();
      console.log('token: ', result.accessToken);
    } catch (error) {
      console.log(error);
    }
  }

  @action.bound handleLogin() {
    this.loginUser();
    this.rootStore.uiStore.toggleLogin();
  }

  async logoutUser() {
    if (this.loggedInUser === undefined) {
      console.log('No user logged in');
    } else {
      await this.client.logout();
      this.loggedInUser = undefined;
      this.rootStore.uiStore.handleLoggedInUser();
      console.log('User logged out successfully!');
    }
  }

  @action.bound handleLogout() {
    this.logoutUser();
  }
}

export default UserStore;
