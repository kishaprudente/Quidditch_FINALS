import { observable, action, runInAction, computed } from 'mobx';

class PlayerStore {
  @observable players = [];
  @observable position = '';

  constructor(rootStore, client) {
    this.rootStore = rootStore;
    this.client = client;
  }

  @action.bound async fetchPlayers() {
    try {
      this.players = [];
      const players = await this.client.service('api/players').find();
      runInAction(() => {
        this.players = players;
      });
    } catch (error) {
      console.log(error);
    }
  }

  init() {
    this.fetchPlayers();
  }
}

export default PlayerStore;
