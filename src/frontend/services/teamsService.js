import transform from '../../hooks/transform';
import Team from '../../models/Team';

function teamsService() {
  return function plugin() {
    const app = this;
    app
      .use('api/teams')
      .service('api/teams')
      .hooks({
        before: {
          find: [],
          get: [],
          create: [],
          update: [],
          patch: [],
          remove: [],
          all: [],
        },
        after: {
          all: [],
          find: [
            transform(Team),
          ],
          get: [],
          create: [],
          update: [],
          patch: [],
          remove: [],
        },
      });
  };
}

export default teamsService;
