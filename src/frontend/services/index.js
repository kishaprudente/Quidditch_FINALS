import gamesService from './gamesService';
import teamsService from './teamsService';
import playersService from './playersService';

function services() {
  return function plugin() {
    const app = this;

    app
      .configure(gamesService())
      .configure(teamsService())
      .configure(playersService());
  };
}

export default services;
