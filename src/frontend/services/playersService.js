import transform from '../../hooks/transform';
import Player from '../../models/Player';

function playersService() {
  return function plugin() {
    const app = this;
    app
      .use('api/players')
      .service('api/players')
      .hooks({
        before: {
          find: [],
          get: [],
          create: [],
          update: [],
          patch: [],
          remove: [],
          all: [],
        },
        after: {
          all: [],
          find: [
            transform(Player),
          ],
          get: [],
          create: [],
          update: [],
          patch: [],
          remove: [],
        },
      });
  };
}

export default playersService;
