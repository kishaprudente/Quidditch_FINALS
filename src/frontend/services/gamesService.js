import transform from '../../hooks/transform';
import Game from '../../models/Game';

function gamesService() {
  return function plugin() {
    const app = this;
    app
      .use('api/games')
      .service('api/games')
      .hooks({
        before: {
          find: [],
          get: [],
          create: [],
          update: [],
          patch: [],
          remove: [],
          all: [],
        },
        after: {
          all: [],
          find: [
            transform(Game),
          ],
          get: [],
          create: [],
          update: [],
          patch: [],
          remove: [],
        },
      });
  };
}

export default gamesService;
