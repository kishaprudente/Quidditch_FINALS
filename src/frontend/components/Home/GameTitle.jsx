import React from 'react';
import { observer } from 'mobx-react';
import Title from 'grommet/components/Title';
import Label from 'grommet/components/Label';

const GameTitle = ({ team }) => (
  <div>
    <Title>
      {team.name}
    </Title>
  </div>
);

export default observer(GameTitle);
