import React from 'react';
import { observer, inject } from 'mobx-react';
import styled from 'styled-components';
import Box from 'grommet/components/Box';
import Section from 'grommet/components/Section';
import Button from 'grommet/components/Button';
import Toast from 'grommet/components/Toast';
import GameTitle from './GameTitle';
import Scoreboard from '../Scoreboard';

const GameContainer = styled(Section)`
  border: 2px solid grey;
  height: 20vh;
  width: 80vw;
`;

const StyledButton = styled(Button)`
  margin: 10px;
`;

const StyledSection = styled(Section)`
border: 2px grey solid;
`;

const Games = ({ rootStore: { uiStore, gameStore }, game }) => (
  <div>
    <GameContainer
      margin="small"
      alignContent="center"
      pad="small"
    >
      {
        game.teams.map(team => <GameTitle key={team._id} team={team} />)
      }
      <Box
        direction="row"
        margin="small"
      >
        {
          uiStore.scoreboardOpen
            ? undefined
            : <StyledButton
              label="Start Game"
              fill={false}
              onClick={
                uiStore.isLoggedIn
                  ? uiStore.toggleScoreboard
                  : uiStore.toggleToast
              }
            />
        }
      </Box>
    </GameContainer>
    {
      uiStore.scoreboardOpen
        ? <StyledSection
          margin="small"
          alignContent="center"
          pad="small"
        >
          <Scoreboard game={game} onLoad={gameStore.manipulateGame()} />
        </StyledSection>
        : undefined
    }
  </div>
);

export default inject('rootStore')(observer(Games));
