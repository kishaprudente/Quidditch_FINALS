import React from 'react';
import { observer, inject } from 'mobx-react';
import Columns from 'grommet/components/Columns';
import Section from 'grommet/components/Section';
import Headline from 'grommet/components/Headline';
import Box from 'grommet/components/Box';
import Button from 'grommet/components/Button';
import Games from './Games';

const Home = ({ rootStore: { gameStore } }) => (
  <Columns
    justify="center"
  >
    <Section
      direction="column"
    >
      <Headline
        align="center"
        margin="medium"
        size="medium"
      >
        Games
      </Headline>
      {
        gameStore.games.map(game => (
          <Games key={game._id} game={game} />
        ))
      }
    </Section>
  </Columns>
);

export default inject('rootStore')(observer(Home));
