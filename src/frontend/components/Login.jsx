import React from 'react';
import { observer, inject } from 'mobx-react';
import styled from 'styled-components';
import Layer from 'grommet/components/Layer';
import Title from 'grommet/components/Tile';
import Header from 'grommet/components/Header';
import FormField from 'grommet/components/FormField';
import TextInput from 'grommet/components/TextInput';
import PasswordInput from 'grommet/components/PasswordInput';
import Button from 'grommet/components/Button';
import Box from 'grommet/components/Box';

const StyledButton = styled(Button)`
  width: 150px;
  height: 40px;
  margin: 5px;
`;

@inject('rootStore') @observer
class Login extends React.Component {
  render() {
    const { rootStore: { uiStore, userStore } } = this.props;
    return (
      <Layer
        closer
      >
        <Box
          margin="small"
          size="large"
        >
          <Header>
            <Title
              tag="h2"
            >
          Login
            </Title>
          </Header>
          <Box
            margin="small"
          >
            <FormField
              label="Username"
              error={userStore.usernameError}
            >
              <TextInput
                onDOMChange={(e) => { userStore.setUsername(e.target.value); }}
              />
            </FormField>
            <FormField
              label="Password"
              error={userStore.passwordError}
            >
              <PasswordInput
                onChange={(e) => { userStore.setPassword(e.target.value); }}
              />
            </FormField>
          </Box>
          <Box
            margin="small"
            direction="row"
          >
            <StyledButton
              label="Login"
              primary
              onClick={userStore.handleLogin}
            />
            <StyledButton
              label="Cancel"
              critical
              fill={false}
              onClick={uiStore.toggleLogin}
            />
          </Box>
        </Box>
      </Layer>
    );
  }
}

export default Login;
