import React from 'react';
import { observer } from 'mobx-react';
import styled from 'styled-components';
import Box from 'grommet/components/Box';
import Button from 'grommet/components/Button';
import Title from 'grommet/components/Title';

const StyledButton = styled(Button)`
  margin: 10px 10px 10px 10px;
`;

const Control = () => (
  <Box
    margin="small"
    direction="column"
    wrap
    basis="1/3"
  >
    <StyledButton
      label="Goal Made"
      primary
      fill
    />
    <StyledButton
      label="Goal Missed"
      primary
      fill
    />
    <StyledButton
      label="Goal Blocked"
      primary
      fill
    />
    <StyledButton
      label="Snitch Appears"
      primary
      fill
    />
    <StyledButton
      label="Snitch Caught"
      primary
      fill
    />
  </Box>
);

export default observer(Control);
