import React from 'react';
import styled from 'styled-components';
import { observer, inject } from 'mobx-react';
import Box from 'grommet/components/Box';
import Value from 'grommet/components/Value';
import Player from './Player';

const StyledValue = styled(Value)`
  display: block !important;
  border: 1px solid grey;
  margin: 3px;
  padding: 3px;
`;

const Team = ({ rootStore: { playerStore }, team }) => (
  <Box
    margin="medium"
    direction="column"
    size="medium"
    basis="1/3"
  >
    <Value
      value={0}
      label={team.name}
    />
  </Box>
);

export default inject('rootStore')(observer(Team));
