import React from 'react';
import { observer, inject } from 'mobx-react';
import styled from 'styled-components';
import Box from 'grommet/components/Box';
import Headline from 'grommet/components/Headline';
import Columns from 'grommet/components/Columns';
import Section from 'grommet/components/Section';
import Control from './Control';
import Team from './Team';

const Scoreboard = ({ rootStore: { gameStore }, game }) => (
  <Box>
    <Headline
      align="center"
      margin="medium"
      size="small"
    >
      Commentator Scoreboard
    </Headline>
    <Columns
      justify="center"
    >
      {
        game.teams.map(team => <Team team={team} key={team._id} />)
      }
    </Columns>
  </Box>
);

export default inject('rootStore')(observer(Scoreboard));