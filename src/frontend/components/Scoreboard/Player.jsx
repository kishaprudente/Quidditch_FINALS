import React from 'react';
import { observer } from 'mobx-react';
import styled from 'styled-components';
import Value from 'grommet/components/Value';
import Label from 'grommet/components/Label';

const StyledValue = styled(Value)`
  display: block !important;
  border: 1px solid grey;
  margin: 3px;
  padding: 3px;
`;

const Player = ({ player }) => (
  <div>
    <Label >{player.lastName}</Label>
    <StyledValue
      value={player.number}
      label={player.position}
      colorIndex="accent-2-t"
      size="small"
      onClick={() => console.log('clicked')}
    />
  </div>
);

export default observer(Player);
