import React from 'react';
import { observer, inject } from 'mobx-react';
import styled from 'styled-components';
import Box from 'grommet/components/Box';
import Menu from 'grommet/components/Menu';
import Anchor from 'grommet/components/Anchor';
import Toast from 'grommet/components/Toast';
import UserAdmin from 'grommet/components/icons/base/UserAdmin';
import Login from './Login';

const MenuIcon = styled(Menu)`
right: 20px !important;
`;

const UserMenu = ({ rootStore: { uiStore, userStore } }) => (
  <Box
    justify="end"
  >
    <MenuIcon
      icon={<UserAdmin />}
      align="end"
      dropAlign={{ left: 'left' }}
    >
      {
        uiStore.isLoggedIn
          ? undefined
          : <Anchor
            label="login"
            onClick={uiStore.toggleLogin}
          />
      }
      <Anchor
        label="logout"
        onClick={
          uiStore.isLoggedIn
            ? userStore.handleLogout
            : uiStore.toggleToast
        }
      />
    </MenuIcon>
    {
      uiStore.loginViewOpen ? <Login /> : undefined
    }
    {
      uiStore.toastViewOpen
        ? <Toast
          status="critical"
        >
            No user logged in.
        </Toast>
        : undefined
    }
  </Box>
);

export default inject('rootStore')(observer(UserMenu));
