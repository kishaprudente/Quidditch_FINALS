import React from 'react';
import { observer, inject } from 'mobx-react';
import { HashRouter as Router, Route } from 'react-router-dom';
import styled from 'styled-components';
import Article from 'grommet/components/Article';
import Header from 'grommet/components/Header';
import Heading from 'grommet/components/Heading';
import Box from 'grommet/components/Box';
import Home from './Home';
import UserMenu from './UserMenu';

const StyledHeading = styled(Heading)`
  left: 20px !important;
`;

@inject('rootStore') @observer
class Root extends React.Component {
  componentDidMount() {
    const { rootStore: { gameStore, teamStore, playerStore } } = this.props;
    gameStore.init();
    teamStore.init();
    playerStore.init();
  }
  render() {
    const { rootStore: { uiStore } } = this.props;
    return (
      <Router>
        <Article>
          <Header
            colorIndex="neutral-3-a"
          >
            <StyledHeading
              uppercase
              tag="h2"
              align="start"
              margin="small"
            >
              Quidditch
            </StyledHeading>
            <UserMenu />
          </Header>
          <Box>
            <Route exact path="/" component={Home} />
          </Box>
        </Article>
      </Router>
    );
  }
}

export default Root;
