import React from 'react';
import { observer } from 'mobx-react';
import styled from 'styled-components';
import Article from 'grommet/components/Article';
import Headline from 'grommet/components/Headline';
import TeamStats from './TeamStats';

const BoxScore = () => (
  <Article>
    <Headline
      align="center"
      margin="medium"
      size="small"
    >
      Box Score
    </Headline>
    <TeamStats />
  </Article>
);

export default observer(BoxScore);
