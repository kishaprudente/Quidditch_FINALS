import React from 'react';
import { observer } from 'mobx-react';
import Box from 'grommet/components/Box';
import Label from 'grommet/components/Label';
import Value from 'grommet/components/Value';
import Table from 'grommet/components/Table';
import TableRow from 'grommet/components/TableRow';

const TeamStats = () => (
  <Box
    pad="small"
  >
    <Box>
      <Value
        value={0}
        label="TEAM NAME HERE"
        align="start"
      />
    </Box>
    <Table>
      <thead>
        <tr>
          <th>
            Player
          </th>
          <th>
            Position
          </th>
          <th>
            Goals Made
          </th>
          <th>
            Goals Missed
          </th>
          <th>
            Goals Blocked
          </th>
          <th>
            Snitch Catched
          </th>
          <th>
            Points
          </th>
        </tr>
      </thead>
      <tbody>
        <TableRow>
          <td>
            Foo
          </td>
          <td className="secondary">
            Chaser
          </td>
          <td className="secondary">
            -
          </td>
          <td className="secondary">
            -
          </td>
          <td className="secondary">
            -
          </td>
          <td className="secondary">
            -
          </td>
          <td className="secondary">
            -
          </td>
        </TableRow>
        <TableRow>
          <td>
            Foo
          </td>
          <td className="secondary">
            Chaser
          </td>
          <td className="secondary">
            -
          </td>
          <td className="secondary">
            -
          </td>
          <td className="secondary">
            -
          </td>
          <td className="secondary">
            -
          </td>
          <td className="secondary">
            -
          </td>
        </TableRow>
        <TableRow>
          <td>
            Foo
          </td>
          <td className="secondary">
            Chaser
          </td>
          <td className="secondary">
            -
          </td>
          <td className="secondary">
            -
          </td>
          <td className="secondary">
            -
          </td>
          <td className="secondary">
            -
          </td>
          <td className="secondary">
            -
          </td>
        </TableRow>
        <TableRow>
          <td>
            Foo
          </td>
          <td className="secondary">
            Keeper
          </td>
          <td className="secondary">
            -
          </td>
          <td className="secondary">
            -
          </td>
          <td className="secondary">
            -
          </td>
          <td className="secondary">
            -
          </td>
          <td className="secondary">
            -
          </td>
        </TableRow>
        <TableRow>
          <td>
            Foo
          </td>
          <td className="secondary">
            Seaker
          </td>
          <td className="secondary">
            -
          </td>
          <td className="secondary">
            -
          </td>
          <td className="secondary">
            -
          </td>
          <td className="secondary">
            -
          </td>
          <td className="secondary">
            -
          </td>
        </TableRow>
      </tbody>
    </Table>
  </Box>
);

export default observer(TeamStats);
