import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'mobx-react';
import { useStrict } from 'mobx';
import './assets/scss/index.scss';
import RootStore from './stores/RootStore';
import Root from './components/Root';
import client from './client';

const rootStore = new RootStore(client);
window.rootStore = rootStore;

useStrict(true);

ReactDOM.render(
  <Provider rootStore={rootStore}>
    <Root />
  </Provider>, document.getElementById('mount-point'));
