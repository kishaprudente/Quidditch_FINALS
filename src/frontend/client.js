import feathersClient from 'feathers/client';
import authClient from 'feathers-authentication-client';
import hooks from 'feathers-hooks';
import socketio from 'socket.io-client';
import feathersSocketio from 'feathers-socketio';

const protocol = location.href.includes('localhost') ? 'http' : 'https';
const url = `${protocol}://${location.host}`;

const client = feathersClient()
  .configure(hooks())
  .configure(feathersSocketio(socketio(url)))
  .configure(authClient({ storage: window.localStorage }))
;

window.app = client;

export default client;
