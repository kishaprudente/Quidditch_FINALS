import Joi from 'joi';
import validate from '../hooks/validate';

class User {
  static get schema() {
    return {
      name: Joi.string().required(),
      gender: Joi.string().required(),
      age: Joi.number().required(),
    };
  }

  constructor(docs) {
    Object.assign(this, docs);
  }

  validate() {
    return Joi.validate(this, this.constructor.schema);
  }

  isValid() {
    return this.validate().error === null;
  }
}

test('Should validate if data is correct', () => {
  const user = new User({ name: 'kisha', gender: 'female', age: 24 });
  const details = {
    type: 'before',
    data: user,
  };
  expect(validate()(details)).toBe(details);
});

test('Should throw error when data is incorrect', () => {
  const user = new User({ name: 123, gender: 12, age: 'two' });
  const details = {
    type: 'before',
    data: user,
  };
  expect(() => validate()(details)).toThrow();
});

test('Should throw error when used as an after hook', () => {
  const user = new User({ name: 'kisha', gender: 'female', age: 24 });
  const details = {
    type: 'after',
    data: user,
  };
  expect(() => validate()(details)).toThrow();
});
