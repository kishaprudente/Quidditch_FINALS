import Team from '../../models/Team';
import Player from '../../models/Player';

describe('Team', () => {
  describe('#score', () => {
    test('Should score points to the team if player makes a goal', () => {
      const newPlayer = { firstName: 'Kisha', lastName: 'Prudente', number: 1, position: 'Chaser' };
      const player = new Player(newPlayer);
      const team = new Team();
      team.players.push(player);
      player.scoreGoal();
      expect(team.score).toEqual(10);
    });
  });
});
