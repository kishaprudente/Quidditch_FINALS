import Player from '../../models/Player';
import Snitch from '../../models/Snitch';

describe('Player', () => {
  describe('#scoreGoal', () => {
    test('Should add a point to goals if a chaser made a goal', () => {
      const newPlayer = { firstName: 'Kisha', lastName: 'Prudente', number: 1, position: 'Chaser' };
      const player = new Player(newPlayer);
      player.scoreGoal();
      expect(player.goalsMade).toEqual(1);
    });

    test('Should throw error when player is not a chaser', () => {
      const newPlayer = { firstName: 'Kisha', lastName: 'Prudente', number: 1, position: 'Seeker' };
      const player = new Player(newPlayer);
      expect(() => player.scoreGoal()).toThrow();
    });
  });

  describe('#missGoal', () => {
    test('Should add a point to goals missed if a chaser missed a goal', () => {
      const newPlayer = { firstName: 'Kisha', lastName: 'Prudente', number: 1, position: 'Chaser' };
      const player = new Player(newPlayer);
      player.missGoal();
      expect(player.goalsMissed).toEqual(1);
    });

    test('Should throw error when player is not a chaser', () => {
      const newPlayer = { firstName: 'Kisha', lastName: 'Prudente', number: 1, position: 'Seeker' };
      const player = new Player(newPlayer);
      expect(() => player.scoreGoal()).toThrow();
    })
  });

  describe('#block', () => {
    test('Should add a point to goals blocked if keeper blocks a goal', () => {
      const newPlayer = { firstName: 'Kisha', lastName: 'Prudente', number: 1, position: 'Keeper' };
      const player = new Player(newPlayer);
      player.blockGoal();
      expect(player.goalsBlocked).toEqual(1);
    });

    test('Should throw error when player is not a keeper', () => {
      const newPlayer = { firstName: 'Kisha', lastName: 'Prudente', number: 1, position: 'Seeker' };
      const player = new Player(newPlayer);
      expect(() => player.blockGoal()).toThrow();
    });
  });

  describe('#catchSnitch', () => {
    test('Should add points to goals if seeker catches the snitch', () => {
      const newPlayer = { firstName: 'Kisha', lastName: 'Prudente', number: 1, position: 'Seeker' };
      const player = new Player(newPlayer);
      const snitch = new Snitch();
      snitch.appeard();
      player.catchSnitch(snitch);
      snitch.caught(player);
      expect(player.goalsMade).toEqual(3);
    });

    test('Should throw error when player is not a seeker', () => {
      const newPlayer = { firstName: 'Kisha', lastName: 'Prudente', number: 1, position: 'Chaser' };
      const player = new Player(newPlayer);
      expect(() => player.catchSnitch()).toThrow();
    });
  });
});
