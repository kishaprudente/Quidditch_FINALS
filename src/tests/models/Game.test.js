import Game from '../../models/Game';

describe('Game', () => {
  describe('#startGame', () => {
    test('Should return time now when game has started', () => {
      const newGame = new Game();
      newGame.startGame();
      const time = newGame.startTime;
      expect(newGame.startTime).toEqual(time);
    });
  });
});
