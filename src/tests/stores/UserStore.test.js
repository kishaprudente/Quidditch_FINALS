import UserStore from '../../frontend/stores/UserStore';

test('should validate username if inputs are correct', () => {
  const username = 'kishamarie';
  const userStore = new UserStore();
  userStore.setUsername(username);
  expect(userStore.values.username).toEqual(username);
});

test('should validate password if inputs are correct', () => {
  const password = 'pass';
  const userStore = new UserStore();
  userStore.setPassword(password);
  expect(userStore.values.password).toEqual(password);
});
