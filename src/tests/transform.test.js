import transform from '../hooks/transform';

test('Should transform result into an instance of a Model when used in a before create method', () => {
  class Model { }
  const details = {
    type: 'before',
    method: 'create',
    data: { foo: 'bar' },
  };
  transform(Model)(details);
  expect(details.data).toBeInstanceOf(Model);
});

test('Should transform result into an instance of a Model when used in an after get method', () => {
  class Model { }
  const details = {
    type: 'after',
    method: 'get',
    result: { foo: 'bar' },
  };
  transform(Model)(details);
  expect(details.result).toBeInstanceOf(Model);
});

test('Should transform all result into an instance of a Model when used in an after find method', () => {
  class Model { }
  const details = {
    type: 'after',
    method: 'find',
    result: [{ foo: 'bar' }, { baz: 'testing' }, { abc: 'efg' }],
  };
  transform(Model)(details);
  expect(details.result[0]).toBeInstanceOf(Model);
  expect(details.result[1]).toBeInstanceOf(Model);
  expect(details.result[2]).toBeInstanceOf(Model);
});
