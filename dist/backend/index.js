'use strict';

var _server = require('./server');

var _server2 = _interopRequireDefault(_server);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const startServer = (() => {
  var _ref = _asyncToGenerator(function* () {
    const app = yield (0, _server2.default)();
    app.listen(process.env.PORT || 8080, function () {
      return console.log('Connected @ PORT:8080');
    });
  });

  return function startServer() {
    return _ref.apply(this, arguments);
  };
})();

startServer();