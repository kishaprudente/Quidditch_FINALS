const path = require('path');
const webpack = require('webpack');

const config = {
  devtool: 'source-map',

  entry: [
    './src/frontend/index.jsx',
  ],

  module: {
    rules: [{
      test: [/\.jsx?$/],
      use: [
        {
          loader: 'babel-loader',
        }],

      exclude:
      /node_modules/,
    },
    {
      test: /\.scss$/,
      use: [
        {
          loader: 'style-loader',
        },
        {
          loader: 'css-loader',
        },
        {
          loader: 'sass-loader',
          options: {
            includePaths: ['./node_modules'],
          },
        },
      ],
    }],
  },


  node: {
    net: 'empty',
  },

  devServer: {
    inline: false,
  },

  resolve: {
    extensions: ['.js', '.jsx', '.scss', '.css'],
  },

  output: {
    path: path.join(process.cwd(), '/public/js/'),
    filename: 'bundle.js',
    publicPath: '/',
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin({
      include: /\.min\.js$/,
      minimize: true,
    }),
  ],
};

module.exports = config;
